import numpy as np
from sklearn import metrics,preprocessing,cross_validation
from sklearn.feature_extraction.text import TfidfVectorizer
import sklearn.linear_model as lm
from sklearn import preprocessing
import pandas as p
import scipy.sparse as sps

traindata = list(np.array(p.read_table('../data/train.tsv'))[:,2])
testdata = list(np.array(p.read_table('../data/test.tsv'))[:,2])
y = np.array(p.read_table('../data/train.tsv'))[:,-1]

tfv = TfidfVectorizer(min_df=3,  max_features=None, strip_accents='unicode',  
analyzer='word',token_pattern=r'\w{1,}',ngram_range=(1, 2), use_idf=1,smooth_idf=1,sublinear_tf=1)

rd = lm.LogisticRegression(penalty='l2', dual=True, tol=0.0001, 
C=1, fit_intercept=True, intercept_scaling=1.0, 
class_weight=None, random_state=None)

X_all = traindata + testdata
lentrain = len(traindata)

print "fitting pipeline"
tfv.fit(X_all)
print "transforming data"
X_all = tfv.transform(X_all)

X = X_all[:lentrain]
X_test = X_all[lentrain:]

featTestX = list(np.array(p.read_table('../data/test.tsv'))[:,[6,19,24,25]])
featTrainX = list(np.array(p.read_table('../data/train.tsv'))[:,[6,19,24,25]])
x = sps.coo_matrix(X)
xtest = sps.coo_matrix(X_test)

n,m = x.shape
n1, m1 = X_test.shape

datatrain = np.zeros(n)
datatrain = np.column_stack([datatrain , [i[1] for i in featTrainX]])
datatrain = np.column_stack([datatrain , [i[3] for i in featTrainX]])

datatest = np.zeros(n1)
datatest = np.column_stack([datatest, [i[1] for i in featTestX]])
datatest = np.column_stack([datatest, [i[3] for i in featTestX]])

scalar = preprocessing.StandardScaler(with_mean=True,with_std=True)
datatest= scalar.fit_transform(datatest)
datatrain = scalar.fit_transform(datatrain)

data_train = sps.coo_matrix(datatrain)
data_test = sps.coo_matrix(datatest)

newx = sps.hstack([x,data_train], format="csr")
newxtest = sps.hstack([xtest,datatest], format="csr")

normalizer = preprocessing.Normalizer().fit(newx)
newx = normalizer.transform(newx)

print "20 Fold CV Score: ", np.mean(cross_validation.cross_val_score(rd,newx,y,cv=20, scoring='roc_auc'))

rd.fit(newx,y)
normalizer = preprocessing.Normalizer().fit(newxtest)
newxtest = normalizer.transform(newxtest)
pred = rd.predict_proba(newxtest)[:,1]
testfile = p.read_csv('../data/test.tsv', sep="\t", na_values=['?'], index_col=1)
pred_df = p.DataFrame(pred, index=testfile.index, columns=['label'])
pred_df.to_csv('benchmark.csv')
print "submission file created.."

